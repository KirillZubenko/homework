const {series, src, dest, watch, parallel} = require('gulp');
const del = require('del');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();


function clean() {
  return del([
    'dist'
  ])
}

function scssCompile() {
  return src('src/scss/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer({cascade: false}))
    .pipe(cleanCSS())
    .pipe(concat('styles.min.css'))
    .pipe(dest('dist/styles'))
}

function jsCompile() {
  return src('src/js/**/*.js')
    .pipe(
      minify({
        noSource: true
      })
    )
    .pipe(
      concat('scripts.min.js')
    )
    .pipe(
      dest('dist/js')
    )
}

function imageOptimise() {
  return src('src/img/**/*.+(svg|png|jpeg|jpg|webp)')
    .pipe(
      imagemin()
    )
    .pipe(
      dest('dist/img')
    )
}

function copyHTML() {
  return src('src/*.html').pipe(
    dest('dist')
  )
}

function reload(done) {
  browserSync.reload();
  done();
}

function server() {
  browserSync.init({
    server: {
      baseDir: './dist'
    }
  });

  watch('src/**/*.js', series(jsCompile, reload));
  watch('src/**/*.scss', series(scssCompile, reload));
  watch('src/img/**/*.+(svg|png|jpeg|jpg|webp)', series(imageOptimise, reload));
  watch('src/*.html').on('change', series(copyHTML, reload));
}

exports.build = series(clean, scssCompile, jsCompile, imageOptimise, copyHTML);
exports.dev = series(exports.build, server);
exports.clean = clean;
exports.default = exports.dev;