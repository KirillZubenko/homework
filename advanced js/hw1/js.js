class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  set name(value) {
    this._name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary;
  }

}
// let test = new Employee('john', 34, 31222)

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  set lang (value) {
    this._lang = value;
  }
  get lang() {
    return this._lang;
  }
  get salary() {
    return this._salary * 3;
  }

}

const workerFrontend = new Programmer('John', 30, 3000, 'eng, ua');
const workerBackend = new Programmer('Mary', 25, 2500, 'it, fr, eng');

console.log(workerBackend);
console.log(workerBackend.salary);
console.log(workerFrontend);
console.log(workerFrontend.salary);
