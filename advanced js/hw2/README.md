### Теоретичні питання
1) Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch  


Загалом використання try catch є доречним, коли нам потрібне виконання коду незважаючи на очікувано отримані помилки в блоці try.  

наприклад, коли намагаємрсь опрацювати дані не існуючим методом, catch перехопить помилку, проте подальший код буде виконано.  

let result = 0;
try {
result = multiply(2, 2);
console.log('це вже не виконується');
} catch(error) {
console.log(error.message);
} 
console.log(result) // виконується

або коли намагаємо спарсити певні дані

const information = '';
try {
const person = JSON.parse(information);
} catch(error) {
console.error(error);
console.log(error.message);
}
console.log('це повідомлення видно');


