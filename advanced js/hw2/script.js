
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

function checkProperties(obj) {
  if (!obj.author) {
    throw new Error(`${obj} should have author`)
  }

  if (!obj.name) {
    throw new Error(`${obj} should have name`)
  }

  if (!obj.price) {
    throw new Error(`${obj} should have price`)
  }

  return true;
}

const filtered = books.filter(book => {
  try {
    checkProperties(book);

  } catch (e) {
    console.error(e);
    return false;
  }
  return true;
});

const root = document.querySelector('#root');
const bookList = document.createElement('ul');
root.append(bookList)
filtered.forEach(element => bookList.appendChild(document.createElement('li')).textContent=JSON.stringify(element));
