const url = 'https://ajax.test-danit.com/api/swapi/films'

async function showMovieInfo() {
  const response = await fetch(url)
  const movies = await response.json();
  // console.log(movies)
  const moviesList = document.querySelector("#movies");
  movies.forEach(({episodeId, name, openingCrawl, characters}) => {
    moviesList.insertAdjacentHTML('afterbegin', `
            <div id="episode-${episodeId}">
                <h3>${episodeId} ${name}</h3>
                <p><span style="font-weight: bold">Short description: </span> ${openingCrawl}</p>
                <p style="font-weight: bold">Characters:</p>
            </div>							
						`);
    // console.log(characters)
    const charactersFetched = Promise.all(
      characters.map(character => {
        return fetch(character).then(r => r.json())
      })
    )
    .then(charactersResponse => {
      charactersResponse.forEach(response => document.querySelector(`#episode-${episodeId}`).children[2].insertAdjacentHTML('afterend', `<span style = 'font-style: italic'>${response.name}, </span>`)
      )
      console.log(charactersResponse)
    })
  })
}

showMovieInfo()
