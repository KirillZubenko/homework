const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
  title = ''
  body = ''
  user = null
  id = ''


  constructor(title, body, user, id) {
    this.title = title;
    this.body = body;
    this.user = user;
    this.id = id;

  }
}

class User {
  name = ''
  email = ''

  constructor(name, email) {
    this.name = name;
    this.email = email;
  }
}

const usersRequest = fetch(usersUrl).then(r => r.json());
const postsRequest = fetch(postsUrl).then(r => r.json());

// const [usersResponse, postsResponse] = Promise.all([usersRequest, postsRequest]);
Promise.all([usersRequest, postsRequest]).then((responses) => {
  const usersResponse = responses[0];
  const postsResponse = responses[1];

  const cards = postsResponse.map(postInResponse => {
    const userInResponse = usersResponse.find(user => user.id === postInResponse.userId);
    const user = new User(userInResponse.name, userInResponse.email);
    return new Card(postInResponse.title, postInResponse.body, user, postInResponse.id);
  });

  cards.forEach((card) => {
    const div = document.createElement('div')
    const title = document.createElement('h4')
    const body = document.createElement('p')
    body.classList.add('body')
    const username = document.createElement('h5')
    const email = document.createElement('p')
    const btn = document.createElement('button')


    title.innerText = card.title
    body.innerText = card.body
    username.innerText = card.user.name
    email.innerText = card.user.email
    btn.innerText = 'Delete'

    btn.onclick = function () {
      const postDeleted = fetch(`https://ajax.test-danit.com/api/json/posts/${card.id}`, {method: 'DELETE'});
      postDeleted.then(()=> {
        div.remove()
      })
    }

    const container = document.getElementById('twitter');
    div.appendChild(title)
    div.appendChild(body)
    div.appendChild(username)
    div.appendChild(email)
    div.appendChild(btn)

    container.appendChild(div)

  })

})