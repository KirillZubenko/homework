const API = 'https://api.ipify.org/?format=json';
const API2 = 'http://ip-api.com/json/';
const btn = document.getElementById('find');


const sendRequest = url => fetch(url).then(r => r.json());

btn.addEventListener('click', async () => {
    const ipResponse = await sendRequest(API);
    const addressResponse = await sendRequest(`${API2}${ipResponse.ip}`);

    document.querySelector('#country-info')?.remove();
    showInfo(addressResponse);
  }
)

function showInfo(info) {
  const div = document.createElement('div');
  div.id = 'country-info'
  div.classList.add('container', 'px-4', 'text-center')
  div.insertAdjacentHTML("afterbegin", `
      <h5>Time Zone: ${info.timezone}</h5>
      <h4>Country: ${info.country}</h4>
      <h3>Region: ${info.regionName}</h3>
      <h2>City: ${info.city}</>
      <h1>Coordinates: lat: ${info.lat} lon: ${info.lon}</h1>
    `);
  document.body.append(div);
}



