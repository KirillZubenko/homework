const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';

class Card {
  title = ""
  body = ''
  user = null


  constructor(title, body, user) {
    this.title = title;
    this.body = body;
    this.user = user;

  }
}

class User {
  name = ""
  email = ""
  constructor(name, email) {
    this.name = name;
    this.email = email;
  }
}

const fetchData = async () => {
  const usersRequest = fetch(usersUrl).then(r => r.json());
  const postsRequest = fetch(postsUrl).then(r => r.json());

  const [usersResponse, postsResponse] = await Promise.all([usersRequest, postsRequest]);


  const postsWithUserData = postsResponse.map(postInResponse => {
    const userInResponse = usersResponse.find(user => user.id === postInResponse.userId);
    const user = new User(userInResponse.name, userInResponse.email);
    return new Card(postInResponse.title, postInResponse.body, user);
  });
  debugger;

    // print each postWithData to page (<div> ... </div>)
  // postsWithUserData.forEach((post) => {
  //
  // })
  // inside each .forEach iteration add a button with click listener to make DELETE request (fetch({method: 'DELETE'})
  // inside click listener delete the element with a given post id
}

fetchData()