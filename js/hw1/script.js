'use strict'

// Теоретичні питання
// 1. Змінну можно оголосити за допомогою const, let або var (застарілий спосіб).
// 2. Функція prompt видає видає модальне окно, в який юзер вводить дані. Функція confirm отримує згоду юзера (ок або ніт) та не має поля для вводу (даних).
// 3. Це процес автоматичного перетворення даних у інший тип даних. Наприклад математичні оператори "- * /" автоматично переводять в числові дані.
//     let firstNum = 5;
//     let secondNum = '1';
//     let result = firstNum - secondNum;
//     console.log(typeof result, result); --> number 4

// Завдання
// 1
let admin;
let name;
name = 'Kirill';
admin = name;
console.log(admin);

// 2
let days = 5;
let secondsInOneDay = 86400;
console.log(days * secondsInOneDay);

//3
let userInfo = prompt('Enter some values');
console.log(userInfo);
