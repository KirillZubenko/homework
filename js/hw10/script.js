
const allTabs = document.querySelector('.tabs');

allTabs.addEventListener('click', (e) => {
    if(e.target.classList.contains('tabs-title')) {
        const clickedTab = e.target.dataset.tab;
        document.querySelector('.tabs-title.active').classList.remove('active');
        e.target.classList.add('active');
        document.querySelector('.tabs-content.active').classList.remove('active');
        document.querySelector(`[data-content=${clickedTab}]`).classList.add('active')
    }
})