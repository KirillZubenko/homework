const form = document.querySelector('.password-form');
form.addEventListener('submit', (e) => {
    e.preventDefault();
})


function addToggleOnClickListener(passwordInputElement, eyeElement) {
    eyeElement.addEventListener('click', () => {
        const type = passwordInputElement.getAttribute('type') === 'password' ? 'text' : 'password';
        passwordInputElement.setAttribute("type", type);
        eyeElement.classList.toggle('fa-eye-slash');
    });
}

const eye1 = document.querySelector('#show');
const pass1 = document.querySelector('#password-input');
const eye2 = document.querySelector('#verify');
const pass2 = document.querySelector('#verify-password-input');

addToggleOnClickListener(pass1, eye1);
addToggleOnClickListener(pass2, eye2);

const submitBtn = document.querySelector('.btn');
submitBtn.addEventListener('click', () => {
    if(pass1.value === pass2.value) {
        alert('You are welcome')
    } else {
        const falsePassword = document.createElement('p');
        falsePassword.innerText = 'Потрібно ввести однакові значення';
        falsePassword.className = 'false-pass';
        document.querySelector('#verify-pass').append(falsePassword);
    }
})