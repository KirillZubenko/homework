// теоретичні питання
// 1. setTimeout викликає функцію один раз через заданий проміжок часу, а setInterval постійно через заданий інтервал.
// 2. вона спрацює майже миттєво, але після виконання коду
// 3. з метою припинення постійного виконання функції

function showImages(images, i = 1) {
    function logic() {
        if (i === 0) {
            images[i].classList.add('active');
        } else if (i === images.length) {
            images[i - 1].classList.remove('active');
            images[0].classList.add('active');
            i = 0;
        } else {
            images[i - 1].classList.remove('active');
            images[i].classList.add('active');
        }
        i++;
    }

    let intervalId = setInterval(logic, 3000)
    const stopBtn = document.getElementById('stop').addEventListener('click', () => {
        clearInterval(intervalId);

    })
    const contBtn = document.getElementById('continue').addEventListener('click', () => {
        clearInterval(intervalId);
        intervalId = setInterval(logic, 3000)

    })

};

const img = document.querySelectorAll('img');
showImages(img);