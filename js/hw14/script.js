const changeTheme = document.querySelectorAll('.change-theme');
console.log(changeTheme)
changeTheme.forEach(switcher => {
    switcher.addEventListener('click', function () {
        themeApply(this.dataset.theme);
        localStorage.setItem('theme', this.dataset.theme)
    })
})

function themeApply(themeName) {
    let themeUrl = `styles/theme-${themeName}.css`;
    document.querySelector('[title="theme"]').setAttribute('href', themeUrl)
}

let activeTheme = localStorage.getItem('theme');
if (activeTheme === null) {
    themeApply('light')
} else {
    themeApply(activeTheme)
}
