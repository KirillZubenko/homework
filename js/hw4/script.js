'use strict'

// Теоретичні питання
// 1. Функції суттєво спрощують життя, зокрема дають багаторазово використовувати один і той же код з різними змінними без дублювання;
// 2. Це список параметрів, які приймає функція, тобто дані з якими вона працює(здійснює певні дії та операції);
// 3.Оператор return повертає значення (результат здійснення функцією заданих дій);
//
//
// Завдання
let userFirstNumber = +prompt('Enter your first number');
let userSecondNumber = +prompt('Enter your second number');
let userMathOperator = prompt('Enter math operation', ['* / + -']);
function calculator(firstNum, secondNum, mathOperation) {
    switch (mathOperation) {
        case '+':
            console.log(firstNum + secondNum);
            break;
        case '-':
            console.log(firstNum - secondNum);
            break;
        case '*':
            console.log(firstNum * secondNum);
            break;
        case '/':
            console.log(firstNum / secondNum);
            break;
        default:
            console.log('Somethig went wrong, make sure you enter the right math operation')
    }
}
calculator(userFirstNumber, userSecondNumber, userMathOperator);
console.log(calculator(3,2,'-'));


