"use strict"
// Теоретичні питання
// 1. екранування дає можливість використовувати символи, які в іншому випадку невірно можуть бути зчитані джсом;
// 2. функція оголошена через слово function + назва, стрілочна функція => присвоєна у значення змінної.
// 3. hoisting це механізм, в якій функції піднімаються в області видимості.

// Завдання
function createNewUser() {
    const firstName = prompt('What is your name?');
    const lastName = prompt('What is your last name?')
    const birthday = prompt('What is your birthdate', 'dd.mm.yy.')
    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge() {
            const now = new Date();
            const birthDate = new Date(birthday);
            return now.getFullYear() - birthDate.getFullYear()
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.slice(-4)
        }
    }
    return newUser;
}
const myLogin = createNewUser();
console.log(myLogin);
console.log(myLogin.getLogin());
console.log(myLogin.getAge());
console.log(myLogin.getPassword())

