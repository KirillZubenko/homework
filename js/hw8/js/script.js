'use strict'
// Теоретичні питання
// 1. dom це дерево всього html документу, сформоване в результаті зчитування браузером html документу.
// 2. innerHTML на відміну від innerText може зчитати теги та внести іх відразу як елементи документу.
// 3. мені подобається querySelector )), зручно бо знаходить будь-який елемент.

// Practice
// 1
const coloredP = document.getElementsByTagName('p');
for (let p of coloredP) {
    p.style.backgroundColor = '#ff0000';
}
//
// // 2
const element = document.getElementById('optionsList');
console.log(element);
console.log(element.parentNode);
console.log(element.childNodes);
//
// // 3 в умові завдання вказано знайти селектор класу такої назви, проте його не існує. За такою назвоню є id
const testPara = document.querySelector('#testParagraph');
testPara.innerHTML = '<p> This is a paragraph </p>';

// 4
const mainHeaders = Array.from(document.querySelectorAll('.main-header'));
console.log(mainHeaders)
mainHeaders.forEach(element => {
    console.log(element.children);
    const headerChildren = Array.from(element.children);
    headerChildren.forEach(headerChild => {
        headerChild.classList.add('nav-item')
    });
})

// 5
const sectionTitle = Array.from(document.querySelectorAll('.section-title'))
console.log(sectionTitle)
sectionTitle.forEach(el => {
    el.classList.remove('section-title')
})
