import React, {Component} from "react";
import Button from "./components/buttons/Button";
import Modal from "./components/modals/Modal";


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOne: false,
      modalTwo: false,
    }
    this.showModalOne = this.showModalOne.bind(this);
    this.showModalTwo = this.showModalTwo.bind(this);
    this.closeModals = this.closeModals.bind(this);

  }


  showModalOne() {
    this.setState({modalOne: true});
  }

  showModalTwo() {
    this.setState({modalTwo: true});
  }

  closeModals() {
    this.setState({
      modalOne: false,
      modalTwo: false,
    })
  }

  render() {

    const actionsModalOne =
      <>
        <Button text="Confirm" backgroundColor="#708090"
        />
        <Button text="Cancel" backgroundColor="#C0C0C0"/>
      </>

    const actionsModalTwo = <Button text="Show more" backgroundColor="#4169E1"/>;


    return (
      <div>
        <Button
          text="Open first modal"
          backgroundColor="#6495ED"
          onClick={this.showModalOne}
        />
        <Button
          text="Open second modal"
          backgroundColor="#FFA500"
          onClick={this.showModalTwo}
        />

        {this.state.modalOne && (
          <Modal
            header="Do you want to delete this file?"
            text="Once you delete this file, it won’t be possible to undo this action. are you sure you want to delete it?"
            closeButton="true"
            actions={actionsModalOne}
            closeModal={this.closeModals}
          />
        )}

        {this.state.modalTwo && (
          <Modal
            header="About us"
            text="This page includes information about our activities.
            If you want to get full information - feel free to press the button bellow."
            closeButton="true"
            actions={actionsModalTwo}
            closeModal={this.closeModals}
          />
        )}
      </div>
    );
  }

}
