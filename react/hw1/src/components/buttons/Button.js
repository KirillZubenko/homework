import React, {Component} from "react";
import "./button.scss";

export default class Button extends Component {
  constructor() {
    super();
  }

  render() {
    const {text, backgroundColor, onClick,} = this.props;
    return (
      <button
        className="button"
        style={{backgroundColor}}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}


