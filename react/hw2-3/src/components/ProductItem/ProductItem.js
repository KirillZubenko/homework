import React, {Component} from 'react';
import './ProductItem.scss'
import Modal from "../Modal/Modal";

class ProductItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
    }
  }

  showModal = () => {
    this.setState({
      isModalOpen: true,
    })
  }

  closeModal = () => {
    this.setState({
      isModalOpen: false,
    })
  }

  addToCart = () => {
    if (this.props.isInCart) {
      this.props.removeFromCart(this.props.item.code)
    } else {
      this.props.addToCart(this.props.item.code)
    }
    this.closeModal();
  }

  addToFav = () => {
    if (this.props.isInFavourites) {
      this.props.removeFromFavourites(this.props.item.code)
    } else {
      this.props.addToFavourites(this.props.item.code)
    }
  }

  render() {

    const actions =
      <>
        <button onClick={this.addToCart}>
          Confirm
        </button>
        <button onClick={this.closeModal}>
          Cancel
        </button>
      </>;


    const {name, url, price, code, color} = this.props.item;

    return (
      <>
        <div className="products__product-item">
          <h3 className="products__product-item-name">{name}</h3>
          <img className="products__product-item-img" src={url} alt={name}/>
          <p className="products__product-item-price">{price} UAH</p>
          <p className="products__product-item-color">Color: {color}</p>
          <div className="products__product-item-actions">
            <button className="products__btn"
                    onClick={this.showModal}>{this.props.isInCart ? 'Remove from cart' : 'Add to cart'}</button>
            <button className="fav-img" onClick={this.addToFav}><img
              src={this.props.isInFavourites ? "/img/favorite-check.png" : "/img/favourite.png"} alt=""/></button>
          </div>

        </div>

        {this.state.isModalOpen && <Modal header="Do you want to add this watch to cart?"
                                          text="If you buy it, you will spend a lot of money"
                                          actions={actions}/>}
      </>

    );
  }
}

export default ProductItem;