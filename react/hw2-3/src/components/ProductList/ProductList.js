import React, {Component} from 'react';
import './ProductList.scss'
import ProductItem from "../ProductItem/ProductItem";

class ProductList extends Component {

  constructor() {
    super();

    this.state = {
      products: [],
    }
  }


  componentDidMount() {
    fetch('/products.json')
      .then(r => r.json())
      .then(products => {
        this.setState({
          products: products,
        })
      })
  }

  render() {
    return (
      <div className="product-list">
        {
          this.state.products.map((product) => {
            const isInFavourites = this.props.favourites.includes(product.code);
            const isInCart = this.props.cart.includes(product.code);
            function a () {
              console.log('hello')
            }
            return <ProductItem isInFavourites={isInFavourites}
                                isInCart={isInCart}
                                item={product}
                                key={product.code}
                                addToCart={this.props.addToCart}
                                removeFromCart={this.props.removeFromCart}
                                addToFavourites={this.props.addToFavourites}
                                removeFromFavourites={this.props.removeFromFavourites}

            />
          })
        }
      </div>
    );
  }
}

export default ProductList;