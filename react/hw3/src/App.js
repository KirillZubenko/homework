import './App.css';
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import CartPage from './components/pages/CartPage'
import FavouritesPage from './components/pages/FavouritesPage'

import {useState} from "react";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

const App = () => {

  const initialStateCart = localStorage.getItem('cart');
  const initialStateFavs = localStorage.getItem('favs');

  const [cart, setCart] = useState(initialStateCart ? JSON.parse(initialStateCart) : [])
  const [favourites, setFavourites] = useState(initialStateFavs ? JSON.parse(initialStateFavs) : [])


  const addToFavourites = (code) => {
    setFavourites([...favourites, code])

    localStorage.setItem('favs', JSON.stringify([...favourites, code]))
  }
  const removeFromFavourites = (code) => {
    setFavourites(favourites.filter((fav) => fav !== code))

    localStorage.setItem('favs', JSON.stringify(favourites.filter((fav) => fav !== code)))
  }

  const addToCart = (code) => {
    setCart([...cart, code])

    localStorage.setItem('cart', JSON.stringify([...cart, code]))
  }

  const removeFromCart = (code) => {
    setCart(cart.filter((cartItem) => cartItem !== code))

    localStorage.setItem('cart', JSON.stringify(cart.filter((cartItem) => cartItem !== code)))
  }


  return (
    <div className="App">
      <Router>


        <Header favourites={favourites} cart={cart}/>
        <Routes>

          <Route path="/" element={<ProductList favourites={favourites}
                                                cart={cart}
                                                addToFavourites={addToFavourites}
                                                removeFromFavourites={removeFromFavourites}
                                                addToCart={addToCart}
                                                removeFromCart={removeFromCart}
          />}/>
          <Route path="/cart" element={<CartPage favourites={favourites}
                                                 cart={cart}
                                                 addToFavourites={addToFavourites}
                                                 removeFromFavourites={removeFromFavourites}
                                                 removeFromCart={removeFromCart}
          />}/>
          <Route path="/favourites" element={<FavouritesPage favourites={favourites}
                                                             cart={cart}
                                                             removeFromFavourites={removeFromFavourites}
                                                             addToCart={addToCart}
                                                             removeFromCart={removeFromCart}
          />}/>

        </Routes>


      </Router>
    </div>
  );

}

export default App;
