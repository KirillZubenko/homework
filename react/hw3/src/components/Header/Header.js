import React from 'react';
import {Link} from 'react-router-dom'
import './Header.scss'

const Header = (props) => {

    return (
      <header className="header">
        <div className="header__logo">
          <img src="/img/logo.png" alt="" />
          <Link to="/"><h1 className="header__title">WATCH SHOP</h1></Link>
        </div>
        <div className="shop-info">
          <Link to="/favourites" className="favorites" >
            <img src="/img/favourite-head.png" alt="fav" />
            {props.favourites.length}
          </Link>
          <Link to="/cart" className="cart">
            <img src="/img/cart.png" alt="cart" />
            {props.cart.length}
          </Link>
        </div>
      </header>
    );
}

export default Header;