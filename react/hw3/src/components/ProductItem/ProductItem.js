import React, {useState} from 'react';
import './ProductItem.scss'
import Modal from "../Modal/Modal";

const ProductItem = (props) => {

  const [isModalOpen, setIsModalOpen] = useState(false)


  const showModal = () => {
    setIsModalOpen(true)
  }

  const closeModal = () => {
    setIsModalOpen(false)
  }

  const addToCart = () => {
    if (props.isInCart) {
      props.removeFromCart(props.item.code)
    } else {
      props.addToCart(props.item.code)
    }
    closeModal();
  }

  const addToFav = () => {
    if (props.isInFavourites) {
      props.removeFromFavourites(props.item.code)
    } else {
      props.addToFavourites(props.item.code)
    }
  }


  const actions =
    <>
      <button onClick={addToCart}>
        Confirm
      </button>
      <button onClick={closeModal}>
        Cancel
      </button>
    </>;


  const {name, url, price, code, color} = props.item;

  return (
    <>
      <div className="products__product-item">
        <h3 className="products__product-item-name">{name}</h3>
        <img className="products__product-item-img" src={url} alt={name}/>
        <p className="products__product-item-price">{price} UAH</p>
        <p className="products__product-item-color">Color: {color}</p>
        <div className="products__product-item-actions">
          <button className="products__btn"
                  onClick={showModal}>{props.isInCart ? 'Remove from cart' : 'Add to cart'}</button>
          <button className="fav-img" onClick={addToFav}><img
            src={props.isInFavourites ? "/img/favorite-check.png" : "/img/favourite.png"} alt=""/></button>
        </div>

      </div>

      {(isModalOpen && !props.isInCart) && <Modal header="Do you want to add this watch to cart?"
                                                text="If you buy it, you will spend a lot of money"
                                                actions={actions}/>}
      {(isModalOpen && props.isInCart) && <Modal header="Do you want to remove this watch from cart?"
                                               text="If you don't buy it, you will save a lot of money"
                                               actions={actions}/>}

    </>

  );
}

export default ProductItem;