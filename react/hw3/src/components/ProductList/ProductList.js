import React, {useState, useEffect} from 'react';
import './ProductList.scss'
import ProductItem from "../ProductItem/ProductItem";

const ProductList = (props) => {


  const [products, setProducts] = useState([])
  const [filteredProducts, setFilteredProducts] = useState([])

  useEffect(() => {
    fetch('/products.json')
      .then(r => r.json())
      .then(products => {
          setProducts(products)
      })
  }, [])

  useEffect(() => {
    const codesToShow = props.codesToShow
    if (codesToShow) {
      const filteredItems = products.filter((product) => {
        return codesToShow.includes(product.code)
      })
      setFilteredProducts(filteredItems)
    } else {
      setFilteredProducts(products)
    }
  },[props.codesToShow, products])

  return (
    <div className="product-list">
      {
        filteredProducts.map((product) => {
          const isInFavourites = props.favourites.includes(product.code);
          const isInCart = props.cart.includes(product.code);

          return <ProductItem isInFavourites={isInFavourites}
                              isInCart={isInCart}
                              item={product}
                              key={product.code}
                              addToCart={props.addToCart}
                              removeFromCart={props.removeFromCart}
                              addToFavourites={props.addToFavourites}
                              removeFromFavourites={props.removeFromFavourites}

          />
        })
      }
    </div>
  );

}

export default ProductList;