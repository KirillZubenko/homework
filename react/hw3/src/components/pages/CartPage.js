import React from 'react';
import ProductList from "../ProductList/ProductList";

const CartPage = (props) => {
    const {favourites, cart, removeFromFavourites, addToFavourites, removeFromCart} = props

    return (
      <div>
        <h1>CART</h1>
        <ProductList favourites={favourites}
                     cart={cart}
                     addToFavourites={addToFavourites}
                     removeFromFavourites={removeFromFavourites}
                     removeFromCart={removeFromCart}
                     codesToShow={cart}
        />
      </div>
    );
}

export default CartPage;