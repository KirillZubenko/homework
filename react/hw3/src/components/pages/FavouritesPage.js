import React from 'react';
import ProductList from "../ProductList/ProductList";

const FavouritesPage = (props) => {

  const {favourites, cart, removeFromFavourites, addToCart, removeFromCart} = props

  return (
    <div>
      <h1>FAVOURITES</h1>
      <ProductList favourites={favourites}
                   cart={cart}
                   removeFromFavourites={removeFromFavourites}
                   addToCart={addToCart}
                   removeFromCart={removeFromCart}
                   codesToShow={favourites}
      />
    </div>
  );
}

export default FavouritesPage;