import { createAction } from '@reduxjs/toolkit';


export const setIsGlobalModalOpen = createAction("SET_IS_MODAL_OPEN");

export const setFavourites = createAction("SET_FAVOURITES");

export const setCart = createAction("SET_CART");

export const setGlobalProducts = createAction("SET_PRODUCTS");




