import './App.css';
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import CartPage from './components/pages/CartPage'
import FavouritesPage from './components/pages/FavouritesPage'

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'



const App = () => {


  return (
    <div className="App">
      <Router>
        <Header/>
        <Routes>
          <Route path="/" element={<ProductList/>}/>
          <Route path="/cart" element={<CartPage/>}/>
          <Route path="/favourites" element={<FavouritesPage/>}/>
        </Routes>
      </Router>
    </div>
  );

}

export default App;
