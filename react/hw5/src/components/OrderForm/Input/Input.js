import React from "react";
import {Field,ErrorMessage} from 'formik';
import './input.scss'



const Input = ({type, label, placeholder, name,error, ...props}) => {
  return (
    <label>
      <p>{label}</p>
      <Field type={type} name={name} {...props} placeholder={placeholder} />
      <ErrorMessage className="error" name={name} component={'p'}/>
    </label>
  )
}

export default Input;