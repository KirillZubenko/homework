import * as yup from 'yup';

export const validation = yup.object().shape({
  firstName: yup
    .string('Enter your name')
    .min(2, 'The name is too short!')
    .required('We should know your name'),

  lastName: yup
    .string('Enter your last name')
    .min(2, 'The last name is too short!')
    .required('We should know your lastname'),
  age: yup
    .number()
    .min(1)
    .positive("You are not that young :)")
    .integer()
    .required('We should know your age'),
  address: yup
    .string()
    .required('We should know your address'),

  phone: yup
    .number('Invalid phone number')
    .positive("A phone number can't start with a minus")
    .integer("A phone number can't include a decimal point")
    .required('Phone is required'),

});
