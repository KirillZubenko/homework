import React, {useState, useEffect} from 'react';
import './ProductList.scss'
import ProductItem from "../ProductItem/ProductItem";
import {setGlobalProducts} from "../../features/actions/actions";
import store from "../../features/store/store";
import {useSelector} from "react-redux";
import {selectCart, selectFavourites, selectProducts} from "../../features/selectors/selectors";

const ProductList = (props) => {

  const products = useSelector(selectProducts)
  const [filteredProducts, setFilteredProducts] = useState([])

  useEffect(() => {
    if(products.length > 0) {
      return
    }
    fetch('/products.json')
      .then(r => r.json())
      .then(products => {
          store.dispatch(setGlobalProducts(products))
      })
  }, [products])

  useEffect(() => {
    const codesToShow = props.codesToShow
    if (codesToShow) {
      const filteredItems = products.filter((product) => {
        return codesToShow.includes(product.code)
      })
      setFilteredProducts(filteredItems)
    } else {
      setFilteredProducts(products)
    }
  },[props.codesToShow, products])

  const cart = useSelector(selectCart)
  const favourites = useSelector(selectFavourites)


  return (
    <div className="product-list">
      {
        filteredProducts.map((product) => {
          const isInFavourites = favourites.includes(product.code);
          const isInCart = cart.includes(product.code);

          return <ProductItem isInFavourites={isInFavourites}
                              isInCart={isInCart}
                              item={product}
                              key={product.code}
                              addToCart={props.addToCart}
                              removeFromCart={props.removeFromCart}

          />
        })
      }
    </div>
  );

}

export default ProductList;