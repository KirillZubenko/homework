import React from 'react';
import ProductList from "../ProductList/ProductList";
import {useSelector} from "react-redux";
import {selectCart} from "../../features/selectors/selectors";
import OrderForm from "../OrderForm/OrderForm";

const CartPage = (props) => {

    const cart = useSelector(selectCart);

    const { removeFromFavourites, addToFavourites, removeFromCart} = props

    return (
      <div>
        <h1>CART</h1>
        <ProductList
                     addToFavourites={addToFavourites}
                     removeFromFavourites={removeFromFavourites}
                     removeFromCart={removeFromCart}
                     codesToShow={cart}
        />
          <OrderForm/>
      </div>

    );
}

export default CartPage;