import React from 'react';
import ProductList from "../ProductList/ProductList";
import {useSelector} from "react-redux";
import {selectCart, selectFavourites} from "../../features/selectors/selectors";

const FavouritesPage = (props) => {
  const favourites = useSelector(selectFavourites);

  const { removeFromFavourites, addToCart, removeFromCart} = props

  return (
    <div>
      <h1>FAVOURITES</h1>
      <ProductList
                   removeFromFavourites={removeFromFavourites}
                   addToCart={addToCart}
                   removeFromCart={removeFromCart}
                   codesToShow={favourites}
      />
    </div>
  );
}

export default FavouritesPage;