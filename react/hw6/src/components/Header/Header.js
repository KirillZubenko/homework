import React, {useEffect} from 'react';
import {Link} from 'react-router-dom'
import './Header.scss'

import {useSelector} from "react-redux";
import {selectCart, selectFavourites} from "../../features/selectors/selectors";

const Header = (props) => {

  const favourites = useSelector(selectFavourites);
  const cart = useSelector(selectCart);

  return (
    <header className="header">
      <div className="header__logo">
        <img src="/img/logo.png" alt=""/>
        <Link to="/"><h1 className="header__title">WATCH SHOP</h1></Link>
      </div>
      <div className="shop-info">
        <Link to="/favourites" className="favorites">
          <img src="/img/favourite-head.png" alt="fav"/>
          {favourites.length}
        </Link>
        <Link to="/cart" className="cart">
          <img src="/img/cart.png" alt="cart"/>
          {cart.length}
        </Link>
      </div>
    </header>
  );
}

export default Header;