import React from "react";
import { Formik } from "formik";
import { useDispatch,useSelector } from 'react-redux';
import {selectCart, selectProducts} from "../../features/selectors/selectors";

import { validation } from "./formValidation";
import {setCart} from '../../features/actions/actions'
import Input from "../OrderForm/Input/Input";

import '../OrderForm/styles/orderForm.scss'

const OrderForm = () => {

  const cart = useSelector(selectCart);
  const products = useSelector(selectProducts)

  const dispatch = useDispatch();

  const showOrder = () => {
    const productsBought = products.filter(
      (product) => {
        return cart.includes(product.code)
      }
    )
    console.log(productsBought)
  }

  return (
    <Formik
      initialValues = {{
        firstName: "",
        lastName: "",
        phone: "",
        age: 16,
        address: "",
      }}
      validationSchema = {validation}
      onSubmit = {(values) => {
        showOrder();
        console.log ('Buyer information:', values)
        dispatch(setCart([]));
        localStorage.setItem('cart', [])
      }}

    >
      {({ errors, touched, handleSubmit }) => (
        <form className = 'order-form' onSubmit={handleSubmit}>
          <legend className='order-form__header'>We need some information about you to get the order delivered </legend>
          <div>
            <Input
              name="firstName"
              placeholder="First name"
              label="First name"
              error={errors.firstName && touched.firstName}
            />
            <Input
              name="lastName"
              placeholder="Last name"
              label="Last name"
              error={errors.lastName && touched.lastName}
            />
            <Input
              name="age"
              type="age"
              placeholder="age"
              label="Age"
              error={errors.age && touched.age}
            />
            <Input
              name="phone"
              type="number"
              placeholder="+380"
              label="Phone"
              error={errors.phone && touched.phone}
            />
            <Input
              name="address"
              placeholder="Country, city, street, postcode"
              label="Address"
              error={errors.address && touched.address}
            />
          </div>
            <button className="btn-checkout">
              CHECKOUT
            </button>
        </form>
      )}
    </Formik>
  );
};

export default OrderForm;
