import React, {useState} from 'react';
import './ProductItem.scss'
import Modal from "../Modal/Modal";
import {useSelector} from "react-redux";
import {selectIsModalOpen} from "../../features/selectors/selectors";
import store from "../../features/store/store";
import {setCart, setFavourites, setIsGlobalModalOpen} from "../../features/actions/actions";

const ProductItem = (props) => {

  const [isModalOpen, setIsModalOpen] = useState(false)


  const showModal = () => {
    setIsModalOpen(true)
    store.dispatch(setIsGlobalModalOpen(true))
  }

  const closeModal = () => {
    setIsModalOpen(false)
    store.dispatch(setIsGlobalModalOpen(false))

  }

  const addToCart = (code) => {
    const cart = store.getState().cart
    store.dispatch(setCart([...cart, code]));

    localStorage.setItem('cart', JSON.stringify([...cart, code]))
  }

  const removeFromCart = (code) => {
    const cart = store.getState().cart
    store.dispatch(setCart(cart.filter((cartItem) => cartItem !== code)))

    localStorage.setItem('cart', JSON.stringify(cart.filter((cartItem) => cartItem !== code)))
  }

  const handleAddToCartClick = () => {
    if (props.isInCart) {
      removeFromCart(props.item.code)
    } else {
      addToCart(props.item.code)
    }
    closeModal();
  }


  const addToFavourites = (code) => {
    const favourites = store.getState().favourites
    store.dispatch(setFavourites([...favourites, code]));


    localStorage.setItem('favs', JSON.stringify([...favourites, code]))
  }
  const removeFromFavourites = (code) => {
    const favourites = store.getState().favourites

    store.dispatch(setFavourites(favourites.filter((fav) => fav !== code)))

    localStorage.setItem('favs', JSON.stringify(favourites.filter((fav) => fav !== code)))
  }

  const handleAddtoFavouritesClicked = () => {
    if (props.isInFavourites) {
      removeFromFavourites(props.item.code)
    } else {
      addToFavourites(props.item.code)
    }
  }

  const actions =
    <>
      <button onClick={handleAddToCartClick}>
        Confirm
      </button>
      <button onClick={closeModal}>
        Cancel
      </button>
    </>;


  const {name, url, price, code, color} = props.item;


  return (
    <>
      <div className="products__product-item">
        <h3 className="products__product-item-name">{name}</h3>
        <img className="products__product-item-img" src={url} alt={name}/>
        <p className="products__product-item-price">{price} UAH</p>
        <p className="products__product-item-color">Color: {color}</p>
        <div className="products__product-item-actions">
          <button className="products__btn"
                  onClick={showModal}>{props.isInCart ? 'Remove from cart' : 'Add to cart'}</button>
          <button className="fav-img" onClick={handleAddtoFavouritesClicked}><img
            src={props.isInFavourites ? "/img/favorite-check.png" : "/img/favourite.png"} alt=""/></button>
        </div>

      </div>

      {(isModalOpen && !props.isInCart) && <Modal header="Do you want to add this watch to cart?"
                                                text="If you buy it, you will spend a lot of money"
                                                actions={actions}/>}
      {(isModalOpen && props.isInCart) && <Modal header="Do you want to remove this watch from cart?"
                                               text="If you don't buy it, you will save a lot of money"
                                               actions={actions}/>}

    </>

  );
}

export default ProductItem;