import { createReducer } from '@reduxjs/toolkit';

import * as actions from '../actions/actions';


export const initialState = {

  isModalOpen: false,

  products: [],

  cart: JSON.parse(localStorage.getItem('cart') || '[]'),

  favourites: JSON.parse(localStorage.getItem('favs') || '[]')

};


export default createReducer(initialState, {

  [actions.setIsGlobalModalOpen]: (state, {payload}) => {
    console.log(state, payload)
    return {...state, isModalOpen: payload} ;

  },


  [actions.setGlobalProducts]: (state, {payload}) => {

    return {...state, products: payload} ;

  },

  [actions.setCart]: (state, {payload}) => {

    return {...state, cart: payload} ;

  },

  [actions.setFavourites]: (state, {payload}) => {

    return {...state, favourites: payload} ;

  }

})
