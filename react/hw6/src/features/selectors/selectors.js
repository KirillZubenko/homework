export const  selectIsModalOpen = (state) => state.isModalOpen;

export const  selectProducts = (state) => state.products;

export const  selectCart = (state) => state.cart;

export const  selectFavourites = (state) => state.favourites;
