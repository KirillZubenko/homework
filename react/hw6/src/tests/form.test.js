import OrderForm from '../components/OrderForm/OrderForm'
import {render, cleanup} from "@testing-library/react";

import store from "../features/store/store";
import {Provider} from 'react-redux';

afterEach(cleanup);

describe('FormComponent', () => {
  it('find input type', () => {
    const {container} = render(<Provider store={store}><OrderForm/></Provider>);
    const nameInput = container.querySelector('[name="firstName"]').type;
    expect(nameInput).toBe("text")
  })

  it('find button submit', () => {
    const {container} = render(<Provider store={store}><OrderForm/></Provider>);
    const lastNameInput = container.querySelector('[value="lastName"]').type;
    expect(lastNameInput).toBe("text")
  })
})

