import rootReducers, {initialState} from "../features/reducers/reducers";
import * as actions from '../features/actions/actions';


describe('testing reducer/actions', () => {
  it('initial state return', () => {
    expect(initialState).toEqual({
      isModalOpen: false,
      products: [],
      cart: [],
      favourites: []
    });
  })

  it('set modal to true', () => {
    expect(rootReducers(initialState, actions.setIsGlobalModalOpen(true)))
      .toEqual({
        isModalOpen: true,
        products: [],
        cart: [],
        favourites: []
      })
  })
})
it('should change cart items', () => {
  const initialState = {
    cart: [222222, 444444, 66666],
  }
  expect(rootReducers(initialState, { type: 'SET_CART', payload: 222222 })).toEqual({ cartList: [444444, 66666] });
  expect(rootReducers(initialState, { type: 'SET_CART', payload: 888888 })).toEqual({ cartList: [222222, 444444, 66666, 888888] });
});

